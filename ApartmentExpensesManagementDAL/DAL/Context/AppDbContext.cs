﻿using ExpenseManagementWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpenseManagementWebApi.DAL.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }
        public DbSet<ApartmentResident> ApartmentResidents { get; set; }
    }
}