﻿using ExpenseManagementWebApi.Models;

namespace ExpenseManagementWebApi.DAL.Repositories
{
    public interface IApartmentRecidentRepository
    {
        ApartmentResident Add(ApartmentResident apartmentResident);
        ApartmentResident Update(ApartmentResident apartmentResident);
        ApartmentResident Delete(int id);
        ApartmentResident Get(int id);
    }
}
