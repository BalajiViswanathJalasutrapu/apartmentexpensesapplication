create database ApartmentExpensesApplicationDB;

create table ApartmentResidents (ResidentID int primary key, FirstName varchar(50) not null, LastName varchar(50) not null, FlatNumber int not null, OwnershipType varchar(50) not null, ContactNumber int not null, Email varchar(50) not null, MoveInDate date not null);
create table MonthlyMaintenance (ID int primary key, ResidentID int constraint my_fk_mm foreign key references ApartmentResidents(ResidentID), month varchar(20) not null, year int not null,  maintenanceamount decimal(10,2) not null, paymentdate date not null, paymentStatus varchar(20) not null, paymentMethodID int not null, CategoryID int unique, Notes varchar(100));
create table ExpenseCategories (ExpenseCategoryID int primary key , CategoryID int constraint my_fk_ec foreign key references MonthlyMaintenance(CategoryID));
create table Users(UserID int primary key, Username varchar(50) not null, password varchar(100) not null, email varchar(100) not null, fullname varchar(100) not null, createdAt datetime not null, updatedAt datetime not null);
create table Tags(TagID int primary key , TagName varchar(50) not null, description varchar(100));
create table PaymentMethods (PaymentMethodID int primary key , PaymentMethodName varchar(50) not null, Description varchar(100), IsActive bit not null);
create table ExpenseTracking (ID int primary key , ExpenseDate date, ExpenseType varchar(50), ExpenseAmount decimal(10,2), Description varchar(100), Rreceipt varchar(max), ResideneceID int);
create table ExpenseTags (ExpenseTagID int primary key , ExpenseID int constraint my_fk_et foreign key references ExpenseTracking(ID), TagID int constraint my_fk_ett foreign key references Tags(TagID));

