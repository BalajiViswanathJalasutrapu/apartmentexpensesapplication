﻿using ExpenseManagementWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpenseManagementWebApi.DAL.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<ApartmentResident> ApartmentResidents { get; set; }
    }
}