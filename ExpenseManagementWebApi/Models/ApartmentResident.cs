﻿using System.ComponentModel.DataAnnotations;

namespace ExpenseManagementWebApi.Models
{
    public class ApartmentResident
    {
        [Key]
        public int ResidentID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public int FlatNumber { get; set; }
        [Required]
        public string OwnershipType { get; set; }
        [Required]
        public int ContactNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime MoveInDate { get; set; }
    }
}
