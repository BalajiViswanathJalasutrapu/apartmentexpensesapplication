﻿using ExpenseManagementWebApi.DAL.Context;
using ExpenseManagementWebApi.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ExpenseManagementWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApartmentResidentController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ApartmentResidentController(AppDbContext context)
        {
            _context = context;
        }
        // GET: api/<ApartmentResidentController>
        [HttpGet]
        public IEnumerable<ApartmentResident> Get()
        {
            return _context.ApartmentResidents;
        }

        // GET api/<ApartmentResidentController>/5
        [HttpGet("{id}")]
        public ApartmentResident Get(int id)
        {
            var resident = _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == id);
            return resident;
        }

        // POST api/<ApartmentResidentController>
        [HttpPost]
        public IActionResult Post([FromBody] ApartmentResident resident)
        {
            _context.ApartmentResidents.Add(resident);
            _context.SaveChanges();
            return Ok();
        }

        // PUT api/<ApartmentResidentController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ApartmentResident updatedResident)
        {
            _context.ApartmentResidents.Add(updatedResident);
            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/<ApartmentResidentController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _context.Remove(id);
            return Ok();
        }
    }
}
