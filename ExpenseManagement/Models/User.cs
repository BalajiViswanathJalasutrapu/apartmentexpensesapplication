﻿namespace ExpenseManagement.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        //[Required]
        //[RegularExpression(@"[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]$")]
        public string Email { get; set; }
        //[Required]
        //[MaxLength(100, ErrorMessage = "FullName Should Not More than 100 characters")]
        public string FullName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
