﻿namespace ApartmentExpensesApplicationProject.Models
{
    public class ExpenseTracking
    {
        public int Id { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string ExpenseType { get; set; }
        public double ExpenseAmount { get; set; }
        public string Description { get; set; }
        public byte Receipt { get; set; }
        public int ResidenceID { get; set; }
    }
}
