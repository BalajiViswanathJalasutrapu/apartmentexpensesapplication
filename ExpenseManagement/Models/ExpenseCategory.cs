﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ApartmentExpensesApplicationProject.Models
{
    public class ExpenseCategory
    {
        public int ExpenseCategoryID { get; set; }
        [ForeignKey("Category")]
        public int CategoryID { get; set; }
    }
}
