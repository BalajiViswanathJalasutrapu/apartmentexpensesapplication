﻿namespace ExpenseManagement.Models
{
    public class ExpenseTag
    {
        public int ExpenseTagID { get; set; }
        public string ExpenseID { get; set; }
        public string TagID { get; set; }
    }
}
