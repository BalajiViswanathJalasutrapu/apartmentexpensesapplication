﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExpenseManagement.Models
{
    public class MonthlyMaintenance
    {
        public int ID { get; set; }
        [ForeignKey("ApartmentResident")]
        public int ResidenceID { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
        public double MaintenanceAmount { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public string PaymentStatus { get; set; }
        public int PaymentMethodID { get; set; }
        public int CategoryID { get; set; }
        public string Notes { get; set; }

        public ApartmentResident ApartmentResident { get; set; }
    }
}
