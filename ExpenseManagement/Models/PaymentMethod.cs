﻿namespace ExpenseManagement.Models
{
    public class PaymentMethod
    {
        public int PaymentMethodId { get; set; }
        public string PaymentMethodName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
