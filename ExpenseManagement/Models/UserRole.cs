﻿namespace ExpenseManagement.Models
{
    public enum UserRole
    {
        owner,
        tenant
    }
}
