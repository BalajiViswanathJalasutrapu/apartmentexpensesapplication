﻿using ExpenseManagement.Data;
using ExpenseManagement.Models;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace ExpenseManagementProject.Controllers
{
    public class ApartmentResidentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ApartmentResidentController(ApplicationDbContext context)
        {
            var url = "https://localhost:44331/api/Values";
            var client = new RestClient(url);

            var request = new RestRequest();
            //var response = client.Get(request);
            _context = context;
        }
        // GET: UserController
        public ActionResult Index()
        {
            var resident = _context.ApartmentResidents;
            return View(resident);
        }

        // GET: UserController/Details/5
        public ActionResult Details(int id)
        {
            var resident = _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == id);
            _context.SaveChanges();
            return View(resident);
        }

        // GET: UserController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        public ActionResult Create(ApartmentResident resident)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.ApartmentResidents.Add(resident);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Edit/5
        public ActionResult Edit(int id)
        {
            var user = _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == id);
            if (user != null)
            {
                return View(user);
            }
            return View();
        }

        // POST: UserController/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ApartmentResident apartmentResident)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.ApartmentResidents.Update(apartmentResident);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ApartmentResident resident)
        {
            try
            {
                _context.Remove(resident);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
