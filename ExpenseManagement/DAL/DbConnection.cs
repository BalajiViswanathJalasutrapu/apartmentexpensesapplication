﻿using Microsoft.Data.SqlClient;

namespace ExpenseManagementWebApi.DAL
{
    public class DbConnection
    {
        protected SqlConnection _sqlConnection = null;
        protected string _connectionString = "Data Source=EPINHYDW07DA\\SQLEXPRESS01;Initial Catalog=ApartmentExpensesApplicationDB;Integrated Security=True";
        //protected string _connectionString = IConfiguration..GetConnectionString("DefaultConnection")

        public DbConnection()
        {

        }
        public DbConnection(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection Connect()
        {
            SqlConnection sqlConnection = new SqlConnection(_connectionString);
            return sqlConnection;
        }
    }
}
