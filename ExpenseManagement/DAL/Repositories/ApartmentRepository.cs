﻿using ExpenseManagement.Data;
using ExpenseManagement.Models;
using ExpenseManagementWebApi.DAL;

namespace ExpenseManagement.DAL.Repositories
{
    public class ApartmentResidentRepository : IApartmentRepository
    {
        private readonly ApplicationDbContext _context;

        public ApartmentResidentRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }
        public ApartmentResident Add(ApartmentResident apartmentResident)
        {
            _context.ApartmentResidents.Add(apartmentResident);
            _context.SaveChanges();
            return apartmentResident;
        }

        public ApartmentResident Delete(int id)
        {
            var apartmentResident = _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == id);
            if (apartmentResident != null)
            {
                _context.ApartmentResidents.Remove(apartmentResident);
                _context.SaveChanges();
            }
            return apartmentResident;
        }

        public ApartmentResident Get(int id)
        {
            return _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == id);
        }

        public ApartmentResident Get()
        {
            DbConnection connection = new DbConnection();
            //SqlCommand command = new SqlCommand(connection.Connect();
        }

        public ApartmentResident Update(ApartmentResident apartmentResident)
        {
            var resident = _context.ApartmentResidents.FirstOrDefault(x => x.ResidentID == apartmentResident.ResidentID);
            if (resident != null)
            {
                _context.ApartmentResidents.Update(resident);
                _context.SaveChanges();
            }
            return resident;
        }
    }
}
