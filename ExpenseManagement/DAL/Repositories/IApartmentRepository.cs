﻿using ExpenseManagement.Models;

namespace ExpenseManagement.DAL.Repositories
{
    public interface IApartmentRepository
    {
        ApartmentResident Add(ApartmentResident apartmentResident);
        ApartmentResident Update(ApartmentResident apartmentResident);
        ApartmentResident Delete(int id);
        ApartmentResident Get(int id);

        ApartmentResident Get();
    }
}
